# leaseplan-fe-assignment

### Website

```shell
    - https://www.leaseplan.com/en-be/business/showroom/
```

### Tech stack

    - Nodejs
    - Webdriverio
    - Cucumber
    - Chai

### Installing and running the project

    - Download or clone the project.
    - Install packages running:

```shell
    npm install
```

    - Run tests using:

```shell
    npx wdio
```
