Feature: Showroom Filters
  Background: Setup
  Given I am on the showroom page

  Scenario Outline: Make
    When I filter by make and select: <make>
    Then I can check all cars are <make>

    # Should come from a database extract or using a TDM approach.
    Examples:
    | make  |
    | AUDI  |
    | VOLVO |
    | MINI  |
    | OPEL  |

  Scenario Outline: Make & Model
    When I filter by make and select: <make>
    And I filter by model: <model>
    Then I can check all cars are <model>

    # Should come from a database extract or using a TDM approach.
    Examples: 
    | make  | model     |
    | AUDI  | A5        |
    | VOLVO | V90       |
    | MINI  | MKIII     |
    | OPEL  | CORSA     |

  Scenario Outline: Make negative
    When I filter by make and select: <make>
    Then I can check all cars are not <otherMake>
  
    # Should come from a database extract or using a TDM approach.
    Examples: 
    | make  | otherMake     |
    | AUDI  | OPEL          |
    | VOLVO | MINI          |

  Scenario Outline: Make & Model negative
    When I filter by make and select: <make>
    And I filter by model: <model>
    Then I can check all cars are not <otherModel>

    # Should come from a database extract or using a TDM approach.
    Examples: 
    | make  | model     | otherModel  |
    | AUDI  | A5        | Q5          |
    | VOLVO | V90       | C40         |
    | MINI  | MKIII     | COUNTRYMAN  |
    | OPEL  | CORSA     | COROLA      |

  Scenario Outline: Price
    When I filter price range between <min> and <max> 
    Then I can check car prices are between <min> and <max>

    Examples: 
    | min     | max     |
    | 400     | 700     |
    | 399     | 600     |

  Scenario Outline: Popular filters
    When I filter by <popularFilter>
    Then I can check cars in the results have <popularFilter> label
    
    Examples:
        | popularFilter   |
        | Best deals      |
        | Fast delivery   | 