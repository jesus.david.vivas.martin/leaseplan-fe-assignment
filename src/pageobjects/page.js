module.exports = class Page {
  get acceptCookiesBtn() {
    return $(".accept-cookies-button");
  }
  open(path) {
    return browser.url(`https://www.leaseplan.com/en-be/${path}`);
  }
};
