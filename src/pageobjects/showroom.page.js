const Page = require("./page");

class ShowroomPage extends Page {
  // eligible for refactor using a common selector strategy <-
  get makeModelDropdown() {
    //returns 2 elements, visible one depends on screen resolution
    return $$('h3[data-key="makemodel"]')[1];
  }

  get popularFilters() {
    //returns 2 elements, visible one depends on screen resolution
    return $$('h3[data-key="popularFilters"]')[1];
  }

  get priceDropdown() {
    //returns 2 elements, visible one depends on screen resolution
    return $$('h3[data-key="monthlyPrice"]')[1];
  }
  // ->

  get carNameElements() {
    return $$('h2[data-component="Heading"]');
  }

  get carPrices() {
    return $$("span[data-component=Heading]");
  }

  get popularFilterLabels() {
    return $$("//div[span[@color]]");
  }

  clickPopularFilter(popularFilter) {
    let filter = popularFilter.replace(" ", "\\\\ ");
    browser.execute(`document.querySelector("#${filter}").click()`);
  }

  clickMakeCheckBox(make) {
    //Using JS click because an element is obscuring the checkbox (div)
    browser.execute(`document.querySelector("#make-${make}").click()`);
  }

  clickModelCheckBox(model) {
    browser.execute(`document.querySelector("#model-${model}").click()`);
  }

  setPriceRange(min, max) {
    return super.open("/business/showroom/?monthlyPrice=" + min + "," + max);
  }

  open() {
    return super.open("/business/showroom/");
  }
}

module.exports = new ShowroomPage();
