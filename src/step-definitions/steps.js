const { Given, When, Then } = require("@cucumber/cucumber");
const expect = require("chai").expect;
const sleep = require("util").promisify(setTimeout);

const ShowroomPage = require("../pageobjects/showroom.page");

const pages = {
  showroom: ShowroomPage,
};

Given(/^I am on the (\w+) page$/, async (page) => {
  await pages[page].open();
  try {
    await ShowroomPage.acceptCookiesBtn.click();
  } catch (e) {}
});

When(/^I filter by make and select: (.*)$/, async (make) => {
  await ShowroomPage.makeModelDropdown.click();
  await ShowroomPage.clickMakeCheckBox(make);
});

When(/^I filter by model: (.*)$/, (model) => {
  ShowroomPage.clickModelCheckBox(model);
});

When(/^I filter by (\w+\ \w+)$/, async (popularFilter) => {
  await ShowroomPage.popularFilters.click();
  await ShowroomPage.clickPopularFilter(popularFilter);
});

When(/^I filter price range between (\w+) and (\w+)/, async (min, max) => {
  //Not necessary since we are setting the price on ?queryparams but still keeping it as real as possible
  await ShowroomPage.priceDropdown.click();
  await ShowroomPage.setPriceRange(min, max);
});

Then(/^I can check all cars are (\w+)$/, async (make) => {
  await sleep(6000);
  const carList = await ShowroomPage.carNameElements;
  for (const e of carList) {
    const carName = await e.getText();
    await expect(carName.toUpperCase()).to.have.string(make);
  }
});

Then(/^I can check all cars are not (\w+)$/, async (make) => {
  await sleep(6000);
  const carList = await ShowroomPage.carNameElements;
  for (const e of carList) {
    const carName = await e.getText();
    await expect(carName.toUpperCase()).to.not.have.string(make);
  }
});

Then(
  /^I can check car prices are between (\w+) and (\w+)$/,
  async (min, max) => {
    const prices = await ShowroomPage.carPrices;
    for (const price of prices) {
      let carPrice = await price.getText();
      carPrice = await carPrice.replace("€", "");
      carPrice = await parseInt(carPrice);
      let minimum = parseInt(min);
      let maximum = parseInt(max);
      await expect(carPrice).to.be.greaterThan(minimum);
      await expect(carPrice).to.be.lessThan(maximum);
    }
  }
);

Then(
  /^I can check cars in the results have (\w+\ \w+) label$/,
  async (popularFilter) => {
    const labels = await ShowroomPage.popularFilterLabels;
    await sleep(5000);
    for (const label of labels) {
      const labelText = await label.getText();
      await expect(labelText).to.have.string(popularFilter);
    }
  }
);
