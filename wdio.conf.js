const { ReportAggregator } = require("wdio-html-nice-reporter");
let reportAggregator;
let bro;

exports.config = {
  specs: ["./src/features/**/*.feature"],
  exclude: [],
  maxInstances: 10,
  capabilities: [
    {
      maxInstances: 5,
      browserName: "chrome",
      "goog:chromeOptions": {
        args: [
          "--disable-background-networking",
          "--enable-features=NetworkService,NetworkServiceInProcess",
          "--disable-background-timer-throttling",
          "--disable-backgrounding-occluded-windows",
          "--disable-breakpad",
          "--disable-client-side-phishing-detection",
          "--disable-component-extensions-with-background-pages",
          "--disable-default-apps",
          "--disable-dev-shm-usage",
          "--disable-extensions",
          "--disable-features=TranslateUI,BlinkGenPropertyTrees",
          "--disable-hang-monitor",
          "--disable-ipc-flooding-protection",
          "--disable-popup-blocking",
          "--disable-prompt-on-repost",
          "--disable-renderer-backgrounding",
          "--disable-sync",
          "--force-color-profile=srgb",
          "--metrics-recording-only",
          "--no-first-run",
          "--enable-automation",
          "--password-store=basic",
          "--use-mock-keychain",
          "--window-size=1920,1080",
          "--headless",
          "--no-sandbox",
        ],
      },
      acceptInsecureCerts: true,
    },
  ],
  logLevel: "error",
  bail: 0,
  baseUrl: "https://www.leaseplan.com/en-be/",
  waitforTimeout: 300000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: ["chromedriver"],
  framework: "cucumber",
  reporters: [
    "spec",
    [
      "html-nice",
      {
        outputDir: "./reports/html-reports/",
        filename: "report.html",
        reportTitle: "Test Report Title",
        useOnAfterCommandForScreenshot: true,
      },
    ],
  ],
  cucumberOpts: {
    require: ["./src/step-definitions/steps.js"],
    backtrace: false,
    requireModule: [],
    dryRun: false,
    failFast: false,
    format: ["pretty"],
    colors: true,
    snippets: true,
    source: true,
    profile: [],
    strict: false,
    tagExpression: "",
    timeout: 120000,
    ignoreUndefinedDefinitions: false,
  },

  //Hooks
  onPrepare: function (config, capabilities) {
    reportAggregator = new ReportAggregator({
      outputDir: "./reports/html-reports/",
      filename: "master-report.html",
      reportTitle: "Master Report",
      collapseTests: true,
      browserName: capabilities.map((i) => i.browserName),
    });

    reportAggregator.clean();
  },

  before: function (capabilities, specs) {
    bro = browser;
    bro.maximizeWindow();
  },
  afterStep: function (step, scenario, result) {
    if (!result.passed) {
      bro.takeScreenshot();
    }
  },
  onComplete: async function (exitCode, config, capabilities, results) {
    (async () => {
      await reportAggregator.createReport();
    })();
  },
};
